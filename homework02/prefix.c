#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "common.h"


void usage(int argc, char** argv);
void verify(int* sol, int* ans, int n);
void prefix_sum(int* src, int* prefix, int n);
void prefix_sum_p1(int* src, int* prefix, int n);
void prefix_sum_p2(int* src, int* prefix, int n);


int main(int argc, char** argv)
{
    // get inputs
    uint32_t n = 1048576;
    unsigned int seed = time(NULL);
    if(argc > 2) {
        n = atoi(argv[1]); 
        seed = atoi(argv[2]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32" elements and time as seed\n", n);
    }


    // set up data 
    int* prefix_array = (int*) AlignedMalloc(sizeof(int) * n);  
    int* input_array = (int*) AlignedMalloc(sizeof(int) * n);
    srand(seed);
    for(int i = 0; i < n; i++) {
        input_array[i] = rand() % 100;
    }


    // set up timers
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // execute serial prefix sum and use it as ground truth
    start_t = ReadTSC();
    prefix_sum(input_array, prefix_array, n);
    end_t = ReadTSC();
    printf("Time to do O(N-1) prefix sum on a %"PRIu32" elements: %g (s)\n", 
           n, ElapsedTime(end_t - start_t));


    // execute parallel prefix sum which uses a NlogN algorithm
    int* input_array1 = (int*) AlignedMalloc(sizeof(int) * n);  
    int* prefix_array1 = (int*) AlignedMalloc(sizeof(int) * n);  
    memcpy(input_array1, input_array, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p1(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do O(NlogN) //prefix sum on a %"PRIu32" elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);

    
    // execute parallel prefix sum which uses a 2(N-1) algorithm
    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p2(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do 2(N-1) //prefix sum on a %"PRIu32" elements: %g (s)\n", 
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);


    // free memory
    AlignedFree(prefix_array);
    AlignedFree(input_array);
    AlignedFree(input_array1);
    AlignedFree(prefix_array1);


    return 0;
}

void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <# elements> <rand seed>\n", argv[0]);
}


void verify(int* sol, int* ans, int n)
{
    int err = 0;
    for(int i = 0; i < n; i++) {
        if(sol[i] != ans[i]) {
            err++;
        }
    }
    if(err != 0) {
        fprintf(stderr, "There was an error: %d\n", err);
    } else {
        fprintf(stdout, "Pass\n");
    }
}

void prefix_sum(int* src, int* prefix, int n)
{
    prefix[0] = src[0];
    for(int i = 1; i < n; i++) {
        prefix[i] = src[i] + prefix[i - 1];
    }
}

void prefix_sum_p1(int* src, int* prefix, int n)
{
<<<<<<< HEAD
	int i;
	int sum = src[0];
	prefix[0] = sum;
	//#pragma omp parallel for
	for (i = 1; i < n; i++) {
		sum = sum + src[i];
		prefix[i] = sum;
}
}
void prefix_sum_p2(int* src, int* prefix, int n)
{
	//printf("first number: %d\n",src[1]);
	
	int d, i;
	omp_set_num_threads(28);
	#pragma omp parallel for
	for (i = 0; i < n; i++) {
		prefix[i] = src[i];
		//printf(" %d", src[i]);
}
	printf("\n");
	//printf("Here: %f\n" , log(10) / log(2));
	//#pragma omp for
	for (d = 0; d < (log(n) / log(2)) - 1; d++) {
		//printf("Here: %d\n", d);
		#pragma omp parallel for
		for (i = pow(2,d)-1; i <= n - 1; i += (int)pow(2,d + 1)) {
			//printf("Checking i: %d\n", i);
			prefix[i + (int)pow(2,d)] = prefix[i] + prefix[i + (int)pow(2,d)];
}
} 

	//printf("\n");
	//prefix[n - 1] = 0;
	//#pragma omp barrier
	for (d = (log(n) / log(2)) - 1; d >= 0; d--) {
		#pragma omp parallel for
		for (i = (int)pow(2,d)-1; i <= n - 1 - (int)pow(2,d); i += (int)pow(2 , d + 1)) {
			if (i - (int)pow(2,d) >= 0) {
				prefix[i] = prefix[i] + prefix[i - (int)pow(2,d)];
		}
	}
}
	prefix[n-1] = src[n-1] + prefix[n-2];
/*
	for ( i = 0; i <= n; i++) {
		printf(" %d",prefix[i]);
}
*/
=======

}

void prefix_sum_p2(int* src, int* prefix, int n)
{

>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10
}



