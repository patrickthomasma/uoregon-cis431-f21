#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <inttypes.h>
#include <time.h>


#define PI 3.1415926535


void usage(int argc, char** argv);
double calcPi_Serial(int num_steps);
<<<<<<< HEAD
double calcPi_MonteCarlo(int num_steps);
=======
>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10
double calcPi_P1(int num_steps);
double calcPi_P2(int num_steps);


int main(int argc, char** argv)
{
    // get input values
<<<<<<< HEAD
    uint32_t num_steps = 100000000;
=======
    uint32_t num_steps = 100000;
>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32"\n", num_steps);
    }
    fprintf(stdout, "The first 10 digits of Pi are %0.10f\n", PI);


    // set up timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // calculate in serial 
    start_t = ReadTSC();
    double Pi0 = calcPi_Serial(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi serially with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi0);
<<<<<<< HEAD


   //calculate in monte carlo
    start_t = ReadTSC();
    double Pi3 = calcPi_MonteCarlo(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi monte carlo with %"PRIu32" steps is: %g\n",
		num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n",Pi3);
=======
>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10
    
    // calculate in parallel with reduce 
    start_t = ReadTSC();
    double Pi1 = calcPi_P1(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi1);


    // calculate in parallel with atomic add
    start_t = ReadTSC();
    double Pi2 = calcPi_P2(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // + atomic with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi2);

    
    return 0;
}


void usage(int argc, char** argv)
{
    fprintf(stdout, "usage: %s <# steps>\n", argv[0]);
}

<<<<<<< HEAD
double calcPi_Serial(int num_steps) {
    double pi = 0.0;
    int i;
    double area = 0;
    double h = 1.0 / num_steps;
    for (i = 1; i <= num_steps; i++) {
	double x = h * (i-0.5);
	area += (4.0 / (1.0 + x*x));
}
	pi = h * area;
	return pi;
}
	

double calcPi_MonteCarlo(int num_steps)
{
    double pi = 0.0;
    double x,y;
    int i;
    int count = 0;
    double z;
    srand(time(NULL));
    for (i = 0; i < num_steps; ++i) {
	x = (double)random()/RAND_MAX;
	y = (double)random()/RAND_MAX;
	z = sqrt((x*x) + (y*y));
	if (z <= 1) {
		++count;
}
}
	pi = ((double)count / (double)num_steps)*4.0;
	
=======
double calcPi_Serial(int num_steps)
{
    double pi = 0.0;
>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10

    return pi;
}

double calcPi_P1(int num_steps)
{
    double pi = 0.0;
<<<<<<< HEAD
    int i;
    double x = 0.0;
    double area = 0;
    double h = 1.0 / num_steps;
    #pragma omp parallel for shared(num_steps,h) reduction(+:area)
    for (i = 1; i <= num_steps; i++) {
        x = h * (i-0.5);
	area += (4.0 / (1.0 + x*x));
}
    pi = h * area;

    return pi;
}
// what
double calcPi_P2(int num_steps)
{
    double pi = 0.0;
    double x = 0.0;
    int i;
    double h = 1.0 / num_steps;
    omp_set_num_threads(28);
    
    #pragma omp parallel shared(num_steps, h)
	{
	double area = 0;
	#pragma omp for
	for (i = 0; i < num_steps; i++) {
		x = h * (i - 0.5);
		area += (4.0/(1.0 + x*x));
}
	#pragma omp atomic
	pi += area;
	   }
 
    return pi * h;
=======

    return pi;
}

double calcPi_P2(int num_steps)
{
    double pi = 0.0;

    return pi;
>>>>>>> b4220497dc217781134625dc48e56f8113ca3e10
}
